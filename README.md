# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

###How to Run ###

  ```
  git clone <yourGitRepository> <targetDirectory>
  ```
  
  ```
  cd <targetDirectory>
  ```
  
  ```
  make run
  ```
  
  *test it with a browser now, while your server is running in a background process*

  ```
  make stop
  ```
  